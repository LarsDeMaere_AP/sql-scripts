use aptunes;
drop procedure if exists MockAlbumReleases

delimiter $$
create procedure MockAlbumReleases(in extraReleases int)
begin
declare counter int default 0;
declare success bool;
callloop : loop
	call MockAlbumReleaseWithSuccess(success);
    if success = 1 then 
		set counter = counter + 1;
	end if;
    if counter = extraReleases then
		leave callloop;
        end if;
end loop;
end$$
delimiter ;
