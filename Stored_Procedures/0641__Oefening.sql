use aptunes;

delimiter $$
create procedure NumberOfGenres(out aantal tinyint)
begin
	select count(*)
    into aantal
    from Genres;
end$$
delimiter ;