use aptunes;

delimiter $$

create procedure CreateAndReleaseAlbum(in titel varchar(100), in bands_Id int)
begin
start transaction;
insert into Albums(Titel)
values(titel);
insert into Albumsreleases (Bands_Id, Albums_Id)
values (bands_Id, last_insert_id());
commit;
end$$

delimiter ;