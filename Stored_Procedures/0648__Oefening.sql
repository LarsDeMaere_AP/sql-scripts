use aptunes;

drop procedure if exists DemonstrateHandlerOrder

delimiter $$
create procedure DemonstrateHandlerOrder()
begin
	declare randomValue tinyint default 0;
    declare continue handler for sqlstate '45002'
    begin
		select 'State 45002 opgevangen. Geen Probleem';
	end;
    declare continue handler for sqlexception
    begin 
		select 'Een algemeen fout opgegeven.';
	end;
    set randomValue = floor(rand() * 3) + 1;
	if randomValue = 1 then
		signal sqlstate '45001';
	elseif randomValue = 2 then
		signal sqlstate '45002';
	elseif randomValue = 3 then
		signal sqlstate '45003';
	end if;
end$$
delimiter ;