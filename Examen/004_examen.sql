use aptunes_examen;

select distinct titel, lengte
from liedjes
inner join liedjesgenres on liedjes.Id = liedjesgenres.Liedjes_Id
inner join genres on liedjesgenres.Genres_Id = 
(
		select Id
        from genres 
        where naam = 'Electronic'
)
order by titel;