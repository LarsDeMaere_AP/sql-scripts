use aptunes_examen;

create user if not exists examen identified by 'examenopdracht';
grant execute on procedure aptunes_examen.GetSongDuration to examen;
grant select on table aptunes_examen.liedjes to examen;