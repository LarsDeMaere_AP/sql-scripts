use aptunes_examen;

delimiter $$

create procedure GetSongDuration(in bandsID int)
begin
declare totalDuration int default 0;
declare songDuration int default 0;
declare ok bool default false;
declare songDurationCursor cursor for select Lengte from liedjes where bandsID = Bands_Id;
declare continue handler for not found set ok = True;
open songDurationCursor;
 fetchloop : loop
	fetch songDurationCursor into songDuration;
    if ok = True then 
		leave fetchloop; 
    end if;
    set totalDuration = totalDuration + songDuration;
 end loop;
 close songDurationCursor;
end$$

delimiter ;