USE ModernWays;

SELECT Studenten.id, Vakken_Id AS Vakken, Cijfer
FROM Studenten
INNER JOIN Evaluaties ON Studenten.id = Evaluaties.Studenten_Id
GROUP BY Studenten.id
HAVING AVG(Evaluaties.Cijfer) > (SELECT AVG(Cijfer) FROM Evaluaties);